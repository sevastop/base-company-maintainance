# Core documentation

## Domain model

For the core application the following domain model is used:

![Domain model](domain-model.png)

## Auth

The core service expects a JWT for authentication. 
As Google Identity Platform (Google IP) suggest using the idToken instead of the accessToken for third party services like this one, we are not using the opaque accessToken.

Google IP does not provide roles as claims in the JWT, so we have to map our own roles inside the service (as far as I understand right now). 
For this purpose, a role enumeration is used to map users roles inside this service. 
The roles are loaded inside the `JobAidAuthorityConverter`. 
https://gitlab.com/jobaid/base/-/blob/main/core/src/main/java/com/jobaidukraine/core/security/JobAidAuthorityConverter.java#L50

To check roles, use the `@PreAuthorize` annotation:

```java
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
@RequestMapping(method = RequestMethod.PATCH, value = "/{id}")
    public EntityModel<Company> update(@RequestBody Company company, @PathVariable long id) throws Exception {
        return null;
    }
```


Simplified auth flow
```plantuml
@startuml
participant "Google IP" as GoogleIP
participant Frontend
participant Core

activate GoogleIP
Frontend -> GoogleIP: Authentication Request
GoogleIP --> Frontend: Authentication Response (AccessToken & idToken)
deactivate GoogleIP

activate Core
Frontend -> Core: GET ressource - auth by idToken
Core -> Core: Validate idToken
Core -> Core: Find or create user by ID from idToken
Core -> Core: Get role from database
Core -> Core: Check role access for ressource
Core -> Frontend: Respond
deactivate Core
@enduml
```
