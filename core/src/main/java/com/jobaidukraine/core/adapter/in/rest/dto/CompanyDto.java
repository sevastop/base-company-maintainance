package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Company", description = "A company that provides jobs")
public class CompanyDto extends ProfileDto {
  @Schema(
      description =
          "The database generated company ID. "
              + "This has to be null when creating a new company. "
              + "This has to be set when updating a company.")
  private Long id;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated creation date of the company. "
              + "This has to be null when creating a new company. ",
      example = "2020-01-01T00:00:00.000Z")
  private LocalDateTime createdAt;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated last update date of the company. "
              + "This has to be null when creating a new company. ",
      example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime updatedAt;

  @Schema(
      description =
          "The database generated version of the company. "
              + "This has to be null when creating a new company.",
      example = "1")
  private Integer version;

  private Boolean deleted;

  @NotBlank(message = "The company name may not be blank or empty.")
  @Schema(description = "The company name.")
  private String name;

  @Pattern(
      regexp =
          "^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$",
      message = "The company website URL is not valid.")
  @Schema(description = "The company url that refers to the official website of the company.")
  private String url;

  @Email(message = "The company email is not valid.")
  @NotBlank(message = "The company email may not be blank or empty.")
  @Schema(description = "The company email.")
  private String email;

  @Schema(description = "The company logo.")
  private String logo;

  @Valid
  @Schema(description = "The job offers of the company.")
  private Set<JobDto> jobs;

  @Valid
  @NotEmpty(message = "The company address may not be empty.")
  @Schema(description = "The company address.")
  private AddressDto address;
}
