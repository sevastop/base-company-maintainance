package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.ProfessionalSkillResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.ProfessionalSkillDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.ProfessionalSkillMapper;
import com.jobaidukraine.core.services.ports.in.queries.ProfessionalSkillQuery;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1.0/skills")
@RequiredArgsConstructor
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "Professional Skill")})
public class ProfessionalSkillController {
  private final ProfessionalSkillQuery professionalSkillQuery;
  private final PagedResourcesAssembler<ProfessionalSkillDto> pagedResourcesAssembler;
  private final ProfessionalSkillResourceAssembler professionalSkillResourceAssembler;
  private final ProfessionalSkillMapper professionalSkillMapper;

  @Operation(
      summary = "Get professional skills from user",
      tags = {"Professional Skill"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Professional Skill retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    array =
                        @ArraySchema(
                            schema = @Schema(implementation = ProfessionalSkillDto.class)))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid pageable or optional title supplied"),
        @ApiResponse(responseCode = "404", description = "No professional skill found")
      })
  @GetMapping
  public PagedModel<EntityModel<ProfessionalSkillDto>> list(
      Pageable pageable, @RequestParam(name = "name") String name) {
    Page<ProfessionalSkillDto> professionalSkills = null;
    professionalSkills =
        professionalSkillQuery
            .findAllByPageable(pageable, name)
            .map(professionalSkillMapper::domainToDto);
    return pagedResourcesAssembler.toModel(professionalSkills, professionalSkillResourceAssembler);
  }
}
