package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import com.jobaidukraine.core.adapter.in.rest.controller.ProfessionalSkillController;
import com.jobaidukraine.core.adapter.in.rest.dto.ProfessionalSkillDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProfessionalSkillResourceAssembler
    implements RepresentationModelAssembler<
        ProfessionalSkillDto, EntityModel<ProfessionalSkillDto>> {
  @Override
  public EntityModel<ProfessionalSkillDto> toModel(ProfessionalSkillDto professionalSkill) {
    try {
      return EntityModel.of(
          professionalSkill,
          linkTo(methodOn(ProfessionalSkillController.class).list(null, ""))
              .withRel("professionalSkills"));
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
