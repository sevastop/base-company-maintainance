package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.ProfileEntity;
import com.jobaidukraine.core.domain.Profile;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbProfileMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {UserMapper.class})
public interface ProfileMapper {
  Profile toDomain(ProfileEntity profileEntity);

  ProfileEntity toEntity(Profile profile);
}
