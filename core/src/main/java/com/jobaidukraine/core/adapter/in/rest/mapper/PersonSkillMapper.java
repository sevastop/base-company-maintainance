package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.PersonSkillDto;
import com.jobaidukraine.core.domain.PersonSkill;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestPersonSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonSkillMapper {
  PersonSkill dtoToDomain(PersonSkillDto personSkillDto);

  PersonSkillDto domainToDto(PersonSkill personSkill);
}
