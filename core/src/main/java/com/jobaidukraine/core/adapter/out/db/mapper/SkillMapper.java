package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.ProfessionalSkillDto;
import com.jobaidukraine.core.adapter.out.db.entities.LanguageSkillEntity;
import com.jobaidukraine.core.adapter.out.db.entities.ProfessionalSkillEntity;
import com.jobaidukraine.core.domain.LanguageSkill;
import com.jobaidukraine.core.domain.ProfessionalSkill;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring", implementationName = "OutDbSkillMapperImpl")
public interface SkillMapper {
  ProfessionalSkill toDomain(ProfessionalSkillEntity professionalSkillDto);

  ProfessionalSkillEntity toEntity(ProfessionalSkill professionalSkill);

  Set<ProfessionalSkill> toDomainSet(Set<ProfessionalSkillEntity> skillEntities);

  Set<ProfessionalSkillEntity> toEntitySet(Set<ProfessionalSkill> professionalSkills);
}
