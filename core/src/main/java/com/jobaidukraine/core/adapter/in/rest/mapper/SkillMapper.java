package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.LanguageSkillDto;
import com.jobaidukraine.core.adapter.in.rest.dto.ProfessionalSkillDto;
import com.jobaidukraine.core.domain.LanguageSkill;
import com.jobaidukraine.core.domain.ProfessionalSkill;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "InRestSkillMapperImpl")
public interface SkillMapper {
  ProfessionalSkill dtoToDomain(ProfessionalSkillDto professionalSkillDto);

  ProfessionalSkillDto domainToDto(ProfessionalSkill professionalSkill);
}
