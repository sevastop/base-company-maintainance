package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<PersonEntity, Long> {}
