package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.PersonController;
import com.jobaidukraine.core.adapter.in.rest.dto.PersonDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PersonResourceAssembler
    implements RepresentationModelAssembler<PersonDto, EntityModel<PersonDto>> {

  @Override
  public EntityModel<PersonDto> toModel(PersonDto person) {
    try {
      return EntityModel.of(
          person,
          linkTo(methodOn(PersonController.class).show(person.getId())).withSelfRel(),
          linkTo(methodOn(PersonController.class).list(null)).withRel("persons"));

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
