package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.ProfessionalSkillDto;
import com.jobaidukraine.core.domain.ProfessionalSkill;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestProfessionalSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProfessionalSkillMapper {
  ProfessionalSkill dtoToDomain(ProfessionalSkillDto professionalSkillDto);

  ProfessionalSkillDto domainToDto(ProfessionalSkill professionalSkill);
}
