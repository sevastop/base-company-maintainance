package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
  @Schema(
      description =
          "The database generated user ID. "
              + "This hast to be null when creating a new user. "
              + "This has to be set when updating an existing user.",
      example = "1")
  private Long id;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated creation date of the language level. "
              + "This has to be null when creating a new language level.",
      example = "2020-01-01T00:00:00.000Z")
  private LocalDateTime createdAt;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated last update date of the job. "
              + "This has to be null when creating a new job.",
      example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime updatedAt;

  @Schema(
      description =
          "The database generated version of the language level. "
              + "This has to be null when creating a new language level.",
      example = "1")
  private Integer version;

  private Boolean deleted;

  @Email(message = "The user email is not valid.")
  @NotBlank(message = "The user email may not be blank or empty.")
  @Schema(description = "The user email.", example = "user@gmail.com")
  private String email;

  @Schema(description = "The person reference.")
  private PersonDto person;

  @Schema(description = "The company reference.")
  private CompanyDto company;
}
