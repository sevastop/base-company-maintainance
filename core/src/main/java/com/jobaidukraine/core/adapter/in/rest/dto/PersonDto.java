package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto extends ProfileDto {
  @Schema(
      description =
          "The database generated person ID. "
              + "This hast to be null when creating a new person. "
              + "This has to be set when updating an existing person.",
      example = "1")
  private Long id;

  @NotNull(message = "The person firstname may not be null.")
  @NotEmpty(message = "The person firstname may not be empty.")
  @Schema(description = "The person firstname.", example = "John")
  private String firstname;

  @NotNull(message = "The person lastname may not be null.")
  @NotEmpty(message = "The person lastname may not be empty.")
  @Schema(description = "The person lastname.", example = "Doe")
  private String lastname;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description = "The date of birthday of the person. ",
      example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime birthday;

  @Valid
  @NotEmpty(message = "The user may not be empty.")
  @Schema(description = "The user object.")
  private UserDto user;

  @Valid
  @Schema(description = "The person's address.")
  private AddressDto address;

  @NotBlank(message = "The type of job may not be blank or empty.")
  @Schema(description = "The type of job.", example = "PART_TIME")
  private JobTypeDto jobType;

  @NotBlank(message = "The max distance may not be blank or empty.")
  @Schema(description = "The max distance in km to the job.", example = "100")
  private int maxDistance;

  @Valid
  @NotNull(message = "The person must have at least one language skill.")
  @Schema(description = "The language skills of a person")
  private Set<LanguageSkillDto> languageSkills;

  @Valid
  @NotNull(message = "The person must have at least one professional skill.")
  @Schema(description = "The professional skills of a person")
  private Set<ProfessionalSkillDto> professionalSkills;
}
