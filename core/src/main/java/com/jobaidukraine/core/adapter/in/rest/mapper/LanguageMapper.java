package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.LanguageSkillDto;
import com.jobaidukraine.core.domain.LanguageSkill;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "InRestLanguageMapperImpl")
public interface LanguageMapper {
  LanguageSkill dtoToDomain(LanguageSkillDto languageSkillDto);

  LanguageSkillDto domainToDto(LanguageSkill languageSkill);
}
