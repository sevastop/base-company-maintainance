package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfessionalSkillDto {
  @Schema(
      description =
          "The database generated skill ID. "
              + "This has to be null when creating a new skill. "
              + "This has to be set when updating a skill.")
  private Long id;

  @NotNull(message = "The skill may not be null.")
  @NotBlank(message = "The skill may not be blank or empty.")
  @Schema(description = "The person skill.")
  private String skillName;
}
