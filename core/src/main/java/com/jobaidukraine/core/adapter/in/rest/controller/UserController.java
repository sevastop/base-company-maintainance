package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.UserResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.UserDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.UserMapper;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.in.queries.UserQuery;
import com.jobaidukraine.core.services.ports.in.usecases.UserUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1.0/users")
@RequiredArgsConstructor
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "User")})
public class UserController {
  private final UserUseCase userUseCase;
  private final UserQuery userQuery;
  private final PagedResourcesAssembler<UserDto> pagedResourcesAssembler;
  private final UserResourceAssembler userResourceAssembler;
  private final UserMapper userMapper;

  @Operation(
      summary = "Create new user",
      tags = {"User"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "User creation was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid user supplied"),
      })
  @PostMapping
  public EntityModel<UserDto> save(@RequestBody UserDto userEntity) {
    User user = this.userUseCase.save(userMapper.dtoToDomain(userEntity));
    return userResourceAssembler.toModel(userMapper.domainToDto(user));
  }

  @Operation(
      summary = "Get all users",
      tags = {"User"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "User retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = UserDto.class)))),
        @ApiResponse(responseCode = "400", description = "Invalid pageable supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "No user found")
      })
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @GetMapping
  public PagedModel<EntityModel<UserDto>> list(Pageable pageable) {
    Page<UserDto> users = userQuery.findAllByPageable(pageable).map(userMapper::domainToDto);
    return pagedResourcesAssembler.toModel(users, userResourceAssembler);
  }

  @Operation(
      summary = "Get user by id",
      tags = {"User"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "User retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "User not found"),
      })
  @GetMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<UserDto> show(@PathVariable long id) {
    //TODO: USER should only be able to get its own information
    User user = this.userQuery.findById(id);
    return userResourceAssembler.toModel(userMapper.domainToDto(user));
  }

  @Operation(
      summary = "Update user by id",
      tags = {"User"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "User update was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid user supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "User not found"),
      })
  @PatchMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<UserDto> update(@RequestBody UserDto userEntity) {
    User user = this.userUseCase.update(userMapper.dtoToDomain(userEntity));
    return userResourceAssembler.toModel(userMapper.domainToDto(user));
  }

  @Operation(
      summary = "Delete user by id",
      tags = {"User"})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "User deletion was successful"),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public void delete(@PathVariable long id) {
    this.userUseCase.delete(id);
  }
}
