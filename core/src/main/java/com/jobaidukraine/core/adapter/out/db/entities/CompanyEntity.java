package com.jobaidukraine.core.adapter.out.db.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Set;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "company")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id",
    scope = CompanyEntity.class)
public class CompanyEntity extends ProfileEntity {
  private String name;
  private String url;
  private String email;
  private String logo;

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "job_id", referencedColumnName = "id", nullable = false)
  @JsonIgnore
  private Set<JobEntity> jobs;

  @Embedded private AddressEntity address;
}
