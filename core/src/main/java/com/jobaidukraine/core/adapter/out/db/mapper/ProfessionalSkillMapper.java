package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.ProfessionalSkillEntity;
import com.jobaidukraine.core.domain.ProfessionalSkill;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbProfessionalSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProfessionalSkillMapper {
  ProfessionalSkill toDomain(ProfessionalSkillEntity professionalSkillEntity);

  ProfessionalSkillEntity toEntity(ProfessionalSkill professionalSkill);
}
