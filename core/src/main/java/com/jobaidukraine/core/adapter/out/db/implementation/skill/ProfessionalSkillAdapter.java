package com.jobaidukraine.core.adapter.out.db.implementation.skill;

import com.jobaidukraine.core.adapter.out.db.mapper.ProfessionalSkillMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.ProfessionalSkillRepository;
import com.jobaidukraine.core.domain.ProfessionalSkill;
import com.jobaidukraine.core.services.ports.out.ProfessionalSkillPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfessionalSkillAdapter implements ProfessionalSkillPort {
  private final ProfessionalSkillRepository professionalSkillRepository;
  private final ProfessionalSkillMapper professionalSkillMapper;

  @Override
  public Page<ProfessionalSkill> findAllByPageable(Pageable pageable, String skillName) {
    return professionalSkillRepository
        .findByNameContainingIgnoreCase(pageable, skillName)
        .map(professionalSkillMapper::toDomain);
  }
}
