package com.jobaidukraine.core.adapter.out.db.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "person")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id",
    scope = PersonEntity.class)
public class PersonEntity extends ProfileEntity {
  private String email;
  private String firstname;
  private String lastname;
  private String position;
  private LocalDateTime birthday;
  private Integer maxDistance;

  private JobTypeEntity jobType;
  @Embedded private AddressEntity address;

  @ElementCollection private Set<LanguageSkillEntity> languageLevels;
  @ElementCollection private Set<ProfessionalSkillEntity> professionalSkills;
}
