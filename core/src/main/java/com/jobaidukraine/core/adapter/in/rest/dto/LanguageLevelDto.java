package com.jobaidukraine.core.adapter.in.rest.dto;

public enum LanguageLevelDto {
  NATIVE,
  FLUENT,
  GOOD,
  BASIC
}
