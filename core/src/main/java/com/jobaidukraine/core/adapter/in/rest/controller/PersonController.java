package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.PersonResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.PersonDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.PersonMapper;
import com.jobaidukraine.core.domain.Person;
import com.jobaidukraine.core.services.ports.in.queries.PersonQuery;
import com.jobaidukraine.core.services.ports.in.usecases.PersonUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1.0/persons")
@RequiredArgsConstructor
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "Person")})
public class PersonController {
  private final PersonUseCase personUseCase;
  private final PersonQuery personQuery;
  private final PagedResourcesAssembler<PersonDto> pagedResourcesAssembler;
  private final PersonResourceAssembler personResourceAssembler;
  private final PersonMapper personMapper;

  @Operation(
      summary = "Create new person",
      tags = {"Person"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful creation of person",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = PersonDto.class))
            }),
        @ApiResponse(responseCode = "400", description = "Invalid person data"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @PostMapping
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<PersonDto> save(@RequestBody PersonDto person) {
    person = personMapper.domainToDto(personUseCase.save(personMapper.dtoToDomain(person)));
    return personResourceAssembler.toModel(person);
  }

  @Operation(
      summary = "Get person by id",
      tags = {"Person"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful retrieval of all persons",
            content = {
              @Content(
                  mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = PersonDto.class)))
            }),
        @ApiResponse(responseCode = "400", description = "Invalid pageable supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "No person found"),
      })
  @GetMapping
  public PagedModel<EntityModel<PersonDto>> list(Pageable pageable) {
    Page<PersonDto> persons =
        personQuery.findAllByPageable(pageable).map(personMapper::domainToDto);
    return pagedResourcesAssembler.toModel(persons, personResourceAssembler);
  }

  @Operation(
      summary = "Get person by id",
      tags = {"Person"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful retrieval of person",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = PersonDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "person not found"),
      })
  @GetMapping(value = "/{id}")
  public EntityModel<PersonDto> show(@PathVariable long id) {
    Person person = personQuery.findById(id);
    return personResourceAssembler.toModel(personMapper.domainToDto(person));
  }

  @Operation(
      summary = "Update person by id",
      tags = {"Person"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful update of person",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = PersonDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id or person supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @PatchMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<PersonDto> update(@RequestBody PersonDto person, @PathVariable long id) {
    person.setId(id);
    person = personMapper.domainToDto(personUseCase.update(personMapper.dtoToDomain(person)));

    return personResourceAssembler.toModel(person);
  }

  @Operation(
      summary = "Delete person by id",
      tags = {"Person"})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Successful deletion of person"),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "Person not found"),
      })
  @DeleteMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable long id) {
    personUseCase.delete(id);
  }
}
