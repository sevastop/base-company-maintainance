package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageSkillDto {

  @Schema(
      description =
          "The database generated language skill ID. "
              + "This hast to be null when creating a new language skill. "
              + "This has to be set when updating an existing language skill.",
      example = "1")
  private Long id;

  @NotBlank(message = "The language may not be blank or empty.")
  @Schema(description = "The language.", example = "English")
  private String language;

  @NotBlank(message = "The language level may not be blank or empty.")
  @Schema(description = "The level of a language.", example = "Native")
  private LanguageLevelDto languageLevel;
}
