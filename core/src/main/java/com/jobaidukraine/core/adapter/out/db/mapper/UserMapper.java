package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.domain.User;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbUserMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {LanguageMapper.class})
public interface UserMapper {
  User toDomain(UserEntity userEntity);

  UserEntity toEntity(User user);
}
