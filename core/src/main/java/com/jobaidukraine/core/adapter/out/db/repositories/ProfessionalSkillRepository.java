package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.ProfessionalSkillEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfessionalSkillRepository extends JpaRepository<ProfessionalSkillEntity, Long> {
  Page<ProfessionalSkillEntity> findByNameContainingIgnoreCase(Pageable pageable, String skillName);
}
