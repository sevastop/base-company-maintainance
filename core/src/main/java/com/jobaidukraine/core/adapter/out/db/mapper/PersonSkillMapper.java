package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.PersonSkillEntity;
import com.jobaidukraine.core.domain.PersonSkill;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbPersonSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonSkillMapper {
  PersonSkill toDomain(PersonSkillEntity personSkillEntity);

  PersonSkillEntity toEntity(PersonSkill personSkill);
}
