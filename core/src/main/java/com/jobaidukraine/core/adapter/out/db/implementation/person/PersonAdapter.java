package com.jobaidukraine.core.adapter.out.db.implementation.person;

import com.jobaidukraine.core.adapter.out.db.entities.PersonEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.PersonMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.PersonRepository;
import com.jobaidukraine.core.domain.Person;
import com.jobaidukraine.core.services.ports.out.PersonPort;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonAdapter implements PersonPort {

  private final PersonRepository personRepository;
  private final PersonMapper personMapper;

  @Override
  public Optional<Person> findById(long id) {
    return personRepository.findById(id).map(personMapper::toDomain);
  }

  @Override
  public Page<Person> findAllByPageable(Pageable pageable) {
    return personRepository.findAll(pageable).map(personMapper::toDomain);
  }

  @Override
  public Person save(Person person) {
    return personMapper.toDomain(personRepository.save(personMapper.toEntity(person)));
  }

  @Override
  public Person update(Person person) {
    PersonEntity personEntity = personRepository.findById(person.getId()).orElseThrow();

    if (person.getFirstname() != null) {
      personEntity.setFirstname(person.getFirstname());
    }
    if (person.getLastname() != null) {
      personEntity.setLastname(person.getLastname());
    }
    if (person.getBirthday() != null) {
      personEntity.setBirthday(person.getBirthday());
    }
    if (person.getMaxDistance() != null) {
      personEntity.setMaxDistance(person.getMaxDistance());
    }

    return personMapper.toDomain(personRepository.save(personEntity));
  }

  @Override
  public void delete(long id) {
    PersonEntity person = personRepository.findById(id).orElseThrow();
    person.setDeleted(true);
    personRepository.save(person);
  }
}
