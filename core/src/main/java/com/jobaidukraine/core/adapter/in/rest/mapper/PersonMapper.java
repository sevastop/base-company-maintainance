package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.PersonDto;
import com.jobaidukraine.core.domain.Person;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestPersonMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      JobMapper.class,
      AddressMapper.class,
      UserMapper.class,
    })
public interface PersonMapper {
  Person dtoToDomain(PersonDto personDto);

  PersonDto domainToDto(Person person);
}
