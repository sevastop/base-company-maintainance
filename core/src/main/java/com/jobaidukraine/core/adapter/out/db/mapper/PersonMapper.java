package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.PersonEntity;
import com.jobaidukraine.core.domain.Person;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbPersonMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class, LanguageMapper.class, ProfessionalSkillMapper.class})
public interface PersonMapper {
  Person toDomain(PersonEntity personEntity);

  PersonEntity toEntity(Person person);
}
