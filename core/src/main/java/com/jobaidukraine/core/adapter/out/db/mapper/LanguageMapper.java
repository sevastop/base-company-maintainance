package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.LanguageSkillEntity;
import com.jobaidukraine.core.domain.LanguageSkill;
import java.util.Set;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbLanguageMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface LanguageMapper {
  LanguageSkill toDomain(LanguageSkillEntity languageSkillEntity);

  LanguageSkillEntity toEntity(LanguageSkill languageSkill);

  Set<LanguageSkill> toDomainSet(Set<LanguageSkillEntity> languageEntities);

  Set<LanguageSkillEntity> toEntitySet(Set<LanguageSkill> languageSkills);
}
