package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.Person;

public interface PersonUseCase {
  Person save(Person person);

  Person update(Person person);

  void delete(long id);
}
