package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.services.ports.in.queries.CompanyQuery;
import com.jobaidukraine.core.services.ports.in.usecases.CompanyUseCase;
import com.jobaidukraine.core.services.ports.out.CompanyPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyService implements CompanyUseCase, CompanyQuery {

  private final CompanyPort companyPort;

  @Override
  public Company save(Company company) {
    return this.companyPort.save(company);
  }

  @Override
  public Page<Company> findAllByPageable(Pageable pageable) {

    return companyPort.findAllByPageable(pageable);
  }

  @Override
  public Company findById(long id) {
    return this.companyPort.findById(id).orElseThrow();
  }

  @Override
  public Company update(Company company) {
    return this.companyPort.update(company);
  }

  @Override
  public void delete(long id) {
    companyPort.delete(id);
  }
}
