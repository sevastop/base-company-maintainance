package com.jobaidukraine.core.services.ports.in.queries;

import com.jobaidukraine.core.domain.ProfessionalSkill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProfessionalSkillQuery {
  Page<ProfessionalSkill> findAllByPageable(Pageable pageable, String skillName);
}
