package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.ProfessionalSkill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProfessionalSkillPort {
  Page<ProfessionalSkill> findAllByPageable(Pageable pageable, String skillName);
}
