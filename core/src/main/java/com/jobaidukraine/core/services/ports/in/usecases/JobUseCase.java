package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.Job;

public interface JobUseCase {
  Job save(Job job);

  Job update(Job job);

  void delete(long id);
}
