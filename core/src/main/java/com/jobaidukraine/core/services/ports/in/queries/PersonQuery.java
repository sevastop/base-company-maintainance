package com.jobaidukraine.core.services.ports.in.queries;

import com.jobaidukraine.core.domain.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersonQuery {
  Person findById(long id);

  Page<Person> findAllByPageable(Pageable pageable);
}
