package com.jobaidukraine.core.services.ports.in.queries;

import com.jobaidukraine.core.domain.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CompanyQuery {
  Company findById(long id);

  Page<Company> findAllByPageable(Pageable pageable);
}
