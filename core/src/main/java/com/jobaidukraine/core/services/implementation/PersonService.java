package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Person;
import com.jobaidukraine.core.services.ports.in.queries.PersonQuery;
import com.jobaidukraine.core.services.ports.in.usecases.PersonUseCase;
import com.jobaidukraine.core.services.ports.out.PersonPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonService implements PersonUseCase, PersonQuery {

  private final PersonPort personPort;

  @Override
  public Person findById(long id) {
    return personPort.findById(id).orElseThrow();
  }

  @Override
  public Page<Person> findAllByPageable(Pageable pageable) {
    return personPort.findAllByPageable(pageable);
  }

  @Override
  public Person save(Person person) {
    return personPort.save(person);
  }

  @Override
  public Person update(Person person) {
    return personPort.update(person);
  }

  @Override
  public void delete(long id) {
    personPort.delete(id);
  }
}
