package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.ProfessionalSkill;
import com.jobaidukraine.core.services.ports.in.queries.ProfessionalSkillQuery;
import com.jobaidukraine.core.services.ports.out.ProfessionalSkillPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfessionalSkillService implements ProfessionalSkillQuery {

  private final ProfessionalSkillPort professionalSkillPort;

  @Override
  public Page<ProfessionalSkill> findAllByPageable(Pageable pageable, String skillName) {
    return professionalSkillPort.findAllByPageable(pageable, skillName);
  }
}
