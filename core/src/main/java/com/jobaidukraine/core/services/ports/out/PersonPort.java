package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.Person;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersonPort {
  Optional<Person> findById(long id);

  Page<Person> findAllByPageable(Pageable pageable);

  Person save(Person person);

  Person update(Person person);

  void delete(long id);
}
