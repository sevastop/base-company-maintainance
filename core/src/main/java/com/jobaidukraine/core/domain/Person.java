package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person extends Profile {
  private String firstname;
  private String lastname;
  private LocalDateTime birthday;
  private JobType jobType;
  private Integer maxDistance;

  private Address address;
  private Set<LanguageSkill> languageSkills;
  private Set<ProfessionalSkill> professionalSkills;
}
