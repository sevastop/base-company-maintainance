package com.jobaidukraine.core.domain;

public enum LanguageLevel {
  NATIVE("NATIVE"),
  FLUENT("FLUENT"),
  GOOD("GOOD"),
  BASIC("BASIC");

  final String level;

  LanguageLevel(String level) {
    this.level = level;
  }

  public String getLevel() {
    return this.level;
  }
}
