package com.jobaidukraine.core.domain;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Company extends Profile {
  private String name;
  private String url;
  private String email;
  private String logo;

  private Set<Job> jobs;
  private Address address;
}
