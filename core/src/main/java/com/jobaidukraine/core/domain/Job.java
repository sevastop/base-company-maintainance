package com.jobaidukraine.core.domain;

import java.time.Instant;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Job {
  private Long id;
  private Instant createdAt;
  private Instant updatedAt;
  private Integer version;
  private Boolean deleted;

  private String title;
  private String description;
  private JobType jobType;
  private String applicationLink;
  private String applicationMail;
  private String videoUrl;

  private Address address;
  private Set<String> qualifications;
  private String classification;
  private Set<LanguageSkill> languageSkills;
}
