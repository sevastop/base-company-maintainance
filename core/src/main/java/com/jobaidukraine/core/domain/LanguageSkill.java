package com.jobaidukraine.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageSkill {
  private String language;
  private LanguageLevel languageLevel;
}
