package com.jobaidukraine.core.domain;

import java.time.Instant;
import lombok.Data;

@Data
public class Housing {
  private Long id;
  private Instant createdAt;
  private Instant updatedAt;
  private Integer version;

  private String title;
  private String description;
  private Integer capacity;

  private Address address;
}
