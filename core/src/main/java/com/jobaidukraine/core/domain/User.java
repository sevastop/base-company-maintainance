package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;

  private String email;

  private Role role;
  private Person person;
  private Company company;
}
