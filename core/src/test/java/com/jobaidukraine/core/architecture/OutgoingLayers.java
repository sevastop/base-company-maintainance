package com.jobaidukraine.core.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import java.util.ArrayList;
import java.util.List;

public class OutgoingLayers extends ArchitectureElement {

  private final HexagonalArchitecture parentContext;
  private final List<String> outgoingLayerPackages = new ArrayList<>();

  OutgoingLayers(HexagonalArchitecture parentContext, String basePackage) {
    super(basePackage);
    this.parentContext = parentContext;
  }

  public OutgoingLayers withPackages(String packageName) {
    this.outgoingLayerPackages.add(fullQualifiedPackage(packageName));
    return this;
  }

  public HexagonalArchitecture and() {
    return parentContext;
  }

  String getBasePackage() {
    return basePackage;
  }

  List<String> getPackages() {
    return outgoingLayerPackages;
  }

  void dontDependOnEachOther(JavaClasses classes) {
    List<String> allOutgoingLayers = outgoingLayerPackages;
    for (String adapter1 : allOutgoingLayers) {
      for (String adapter2 : allOutgoingLayers) {
        if (!adapter1.equals(adapter2)) {
          denyDependency(adapter1, adapter2, classes);
        }
      }
    }
  }

  void doesNotDependOn(String packageName, JavaClasses classes) {
    outgoingLayerPackages.forEach(outgoing -> denyDependency(outgoing, packageName, classes));
  }

  void doesNotContainEmptyPackages() {
    denyEmptyPackages(outgoingLayerPackages);
  }
}
