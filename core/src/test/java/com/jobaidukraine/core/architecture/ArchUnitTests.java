package com.jobaidukraine.core.architecture;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.jobaidukraine.core.CoreApplication;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;
import org.junit.jupiter.api.Test;
import org.mapstruct.Mapper;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

@AnalyzeClasses(
    packagesOf = CoreApplication.class,
    importOptions = ImportOption.DoNotIncludeTests.class)
class ArchUnitTests {
  @ArchTest
  ArchRule noMembersShouldBeAutowired = GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION;

  @ArchTest ArchRule noJodaTime = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME;

  @ArchTest
  ArchRule noGenericException = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

  @ArchTest
  ArchRule serviceClassAnnotation =
      classes()
          .that()
          .areTopLevelClasses()
          .and()
          .areNotInterfaces()
          .and()
          .resideInAPackage("..services.implementation..")
          .should()
          .beMetaAnnotatedWith(Service.class);

  @ArchTest
  ArchRule restControllerClassAnnotation =
      classes()
          .that()
          .areTopLevelClasses()
          .and()
          .areNotInterfaces()
          .and()
          .resideInAPackage("..adapter.in.rest.controller..")
          .should()
          .beMetaAnnotatedWith(RestController.class);

  @ArchTest
  ArchRule mapperClassAnnotation =
      classes()
          .that()
          .areTopLevelClasses()
          .and()
          .areInterfaces()
          .and()
          .resideInAPackage("..mapper..")
          .should()
          .beMetaAnnotatedWith(Mapper.class);

  @ArchTest
  ArchRule configurationClassesAnnotation =
      classes()
          .that()
          .areTopLevelClasses()
          .and()
          .areNotInterfaces()
          .and()
          .resideInAPackage("..configuration..")
          .should()
          .beMetaAnnotatedWith(Configuration.class);

  @ArchTest
  ArchRule interfaces =
      classes()
          .that()
          .areTopLevelClasses()
          .and()
          .resideInAPackage("..adapter.out.db.repositories..")
          .and()
          .resideInAPackage("..services.ports..")
          .should()
          .beInterfaces()
          .allowEmptyShould(true);

  @ArchTest
  ArchRule domainLayerDoesNotDependOnApplicationLayer =
      noClasses()
          .that()
          .resideInAPackage("..domain..")
          .should()
          .dependOnClassesThat()
          .resideInAnyPackage("..application..")
          .allowEmptyShould(true);

  @Test
  void validateRegistrationContextArchitecture() {
    HexagonalArchitecture.boundedContext("com.jobaidukraine.core")
        .withDomainLayer("domain")
        .withAdaptersLayer("adapter")
        .incoming("in.rest")
        .outgoing("out.db")
        .and()
        .withApplicationLayer("services")
        .services("implementation")
        .incomingPorts("ports.in")
        .outgoingPorts("ports.out")
        .and()
        .withOutgoingLayer("adapter.out")
        .withPackages("db.repositories")
        .withPackages("rest.client")
        .and()
        .withIncomingLayer("adapter.in")
        .withPackages("rest.controller")
        .and()
        .withConfiguration("configuration")
        .withServicesLayer("services.implementation")
        .check(
            new ClassFileImporter()
                .withImportOption(new ImportOption.DoNotIncludeTests())
                .importPackages("com.jobaidukraine.core.."));
  }
}
