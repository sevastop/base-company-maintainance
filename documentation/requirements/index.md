# Requirements
## Intro
This document can be used to get a basic overview about the requirements
in the JobAidUkraine project. 

As real work items in this project are stories and epics, diagrams and texts 
in this requirements folder should only be the base
to derive work items from.


## System overview proposal
![System Overview](../files/architecture_proposal_jobaid.drawio.png)


## Activities
```plantuml
@startuml
!theme plain

left to right direction
skinparam packageStyle rectangle
actor refugee
actor company
actor admin

rectangle JobAid-System {
    company --> (Create job postings)
    company --> (Create account)
    company --> (Delete job posting)
    company --> (Mark job posting as filled)
    company --> (Login)
    company --> (Logout)
    company --> (Activate posting expiration mail)
    company --> (Submit job posting via API)

    refugee --> (Create account)
    refugee --> (Delete account)
    refugee --> (Login)
    refugee --> (Fill out profile)
    refugee --> (Show all jobs)
    refugee --> (Search job by location)
    refugee --> (Get job recommendations)

    admin --> (Login)
    admin --> (Disable job postings)
    admin --> (Delete job postings)
    admin --> (Disable accounts)
    admin --> (Manage bad words)
}
@enduml
```

## Post a job flow

```plantuml
@startuml
!theme plain


start

if ( User registered ) then (yes)
    :login;
else (no)
    :Fill out registration form;
    :Send confirmation Mail;
    stop;
endif;

:Fill out job form;
:Validate job form;
:Submit job;
:Scan for bad words;
if ( bad words detected) then (yes)
    :add to manual approval list;
else
    :Activate job posting;
endif;

partition "V2(AI model)" {
    :Context analyse job post;
    :Extract required skills;
    :Make job available for matching;
}
stop

@enduml
```

