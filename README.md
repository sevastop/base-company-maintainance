![JobAid Ukraine Logo](https://gitlab.com/jobaid/base/-/raw/main/frontend/assets/Logo_JobAidUkraine.svg "JobAid Ukraine Logo")

# JobAid Base Repository

[![Slack](https://img.shields.io/badge/Slack-JobAid%20Tech-0057B7.svg?logo=slack)](https://join.slack.com/t/jobaidtech/shared_invite/zt-15aekfxir-BrpdzPzhw3hnnqg_MEcHmA)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-FFDD00.svg)](https://gitlab.com/jobaid/base/-/tree/main/CODE_OF_CONDUCT.md)
[![License](https://img.shields.io/badge/License-MIT-0057B7.svg)](https://gitlab.com/jobaid/base/-/tree/main/LICENSE)

The JobAid base project includes all source code and dependencies that is needed to run the jobAid platform.

## Contact

If you want to chat about the project from a technical perespective, join our [Slack channel](https://join.slack.com/t/jobaidtech/shared_invite/zt-15aekfxir-BrpdzPzhw3hnnqg_MEcHmA).

## License

[You can find the license here](https://gitlab.com/jobaid/base/-/blob/main/LICENSE)

## Architecture

[Architecture proposal](https://gitlab.com/jobaid/base/-/blob/main/Architecture.md)

## Get Involved

You're invited to join this project! Check out the [contributing guide](https://gitlab.com/jobaid/base/-/blob/main/CONTRIBUTING.md).
Regarding to each service you may find other contributing guides.

If you're interested in how the project is organized at a higher level, please join our [slack](https://join.slack.com/t/jobaidtech/shared_invite/zt-15aekfxir-BrpdzPzhw3hnnqg_MEcHmA)!

## Quickstart guides

You can find quick start guides for every service in the corresponding service folder.

1. [Core Service](./core/README.md)
1. [Frontend](./frontend/README.md)
1. [Geo-Service](./geo-service/README.md)
1. [CMS](./cms/README.md)

## Guidelines for visual design

When you want to extend the JobAidUkraine frontend, please have a look at our [design guidelines](https://xd.adobe.com/view/9f91c241-dfde-4654-b4b4-1bcfa87e9371-cac2/).  
