import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    modules: ['@nuxtjs/axios'],
    buildModules: ['@nuxtjs/tailwindcss'],
    publicRuntimeConfig: {
        apiUrl: process.env.API_URL,
        apiUrlGeo: process.env.API_URL_GEO,
      },
})


