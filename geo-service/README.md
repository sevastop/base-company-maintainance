# Geo Service

This service provides a very simple API to match ZIPs and town names to coordinates.

Endpoints are: 
1. `/zip/:zip`: Get coordinates for towns of the zip
1. `/town/:town`: Get coordinates for towns matching the given string.

To start the app, just use `npm run dev`, or build it via docker.

This app exposes port `3001`. 